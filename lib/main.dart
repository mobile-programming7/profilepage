import 'package:flutter/material.dart';
enum APP_THEME{LIGHT,DARK}

void main() {
  runApp(ContactProfilePage());
}

class MyAppTheme {
  static ThemeData appThemeLight(){
    return ThemeData(
      brightness: Brightness.light,
      appBarTheme: AppBarTheme(
        color: Colors.lightBlueAccent,
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.indigo.shade800,
        ),
      );
    }

  static ThemeData appThemeDark(){
    return ThemeData(
      brightness: Brightness.dark,
      appBarTheme: AppBarTheme(
        color: Colors.black12,
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.indigo.shade100,
      ),
    );
  }
}

class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {

  var currenTheme = APP_THEME.LIGHT;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currenTheme == APP_THEME.DARK
        ? MyAppTheme.appThemeLight()
        : MyAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: appBarMain(),
        body: bodyMain(),
        floatingActionButton: FloatingActionButton (
          child: Icon(Icons.light_mode),
          onPressed: (){
            setState(() {
              currenTheme == APP_THEME.DARK
                  ? currenTheme == APP_THEME.LIGHT
                  : currenTheme == APP_THEME.DARK;
            });
          },
        ),
      ),
    );
  }

  bodyMain(){
    return ListView(
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              //Height constraint at Container widget level
              height: 250,
              child: Image.network(
                "https://www.len.game/wp-content/uploads/2022/09/image-3-1024x640.jpeg",
                fit: BoxFit.cover,
              ),
            ),
            Container(
              height: 68,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,

                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      "Vergil",
                      style: TextStyle(fontSize: 30),
                    ),
                  ),
                ],
              ),
            ),
            Divider(
              color: Colors.lightBlueAccent,
            ),
            Container(
              margin: const EdgeInsets.only(top: 8, bottom: 8),
              child: Theme(
                data: ThemeData(
                  iconTheme: IconThemeData(
                    color: Colors.lightBlue,
                  ),
                ),
                  child: profileActionItem()
              )
            ),
            Divider(
              color: Colors.lightBlue,

            ),
            mobilePhoneListfile(),
            Divider(
              color: Colors.lightBlue,
            ),
            mobilePhoneListfile2(),
            mobilePhoneListfileEmail(),
            mobilePhoneListfileLocation(),
          ],
        ),
      ],
    );
  }

  Widget profileActionItem(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        buildCallButton(),
        buildTextButton(),
        buildVideoCallButton(),
        buildEmailButton(),
        buildDirectionButton(),
        buildPayButton(),
      ],
    );
  }

  Widget buildCallButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.call,
            //color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Call"),
      ],
    );
  }

  Widget buildTextButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.textsms,
            //color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Text"),
      ],
    );
  }

  Widget buildVideoCallButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.video_camera_back,
            //color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Video"),
      ],
    );
  }

  Widget buildEmailButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.email,
            //color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Email"),
      ],
    );
  }

  Widget buildDirectionButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.directions,
            //color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Direction"),
      ],
    );
  }

  Widget buildPayButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.payment,
            //color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Pay"),
      ],
    );
  }
}

Widget mobilePhoneListfile(){
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("330-880-3390"),
    subtitle: Text("mobile"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

Widget mobilePhoneListfile2(){
  return ListTile(
    leading: Text(""),
    title: Text("440-440-3390"),
    subtitle: Text("other"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

Widget mobilePhoneListfileEmail(){
  return ListTile(
    leading: Icon(Icons.email),
    title: Text("vergil@vergil.com"),
    subtitle: Text("work"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

Widget mobilePhoneListfileLocation(){
  return ListTile(
    leading: Icon(Icons.location_on),
    title: Text("234 Sunset St.Burlingame"),
    subtitle: Text("home"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

appBarMain(){
  return AppBar(
    //backgroundColor: Colors.lightBlueAccent,
    title: Text("Page"),
    leading: Icon(
      Icons.arrow_back,
      //color: Colors.white,
    ),
    actions: <Widget>[
      IconButton(
        icon: Icon(Icons.star_border),
        //color: Colors.black,
        onPressed: (){
          print("Contract is starred");
        },
      ),
    ],
  );
}




